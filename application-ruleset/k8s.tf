locals {
  k8s_rules = {
    haproxy_ingress = {
      service   = "haproxy"
      direction = "ingress"
      protocol  = "tcp"
      ports     = [8080, 9090]
      target    = "public"
    }
    k8sapi_ingress = {
      service   = "kubernetes"
      direction = "ingress"
      protocol  = "tcp"
      ports     = [6443]
      target    = "public"
    }
  }
}