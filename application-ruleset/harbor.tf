locals {
  harbor_rules = {
    harbor_http_ingress = {
      service   = "harbor"
      direction = "ingress"
      protocol  = "tcp"
      ports     = [8080]
      target    = "network"
    }
    harbor_https_ingress = {
      service   = "harbor"
      direction = "ingress"
      protocol  = "tcp"
      ports     = [443]
      target    = "public"
    }
  }
}