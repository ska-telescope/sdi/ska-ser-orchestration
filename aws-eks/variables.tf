variable "defaults" {
  type = object({
    cluster_name    = string
    cluster_version = string
    aws_auth_roles = optional(list(
      object({
        rolearn  = string
        username = string
        groups   = list(string)
    })))
    cluster_enabled_log_types      = optional(list(string))
    cluster_endpoint_public_access = optional(bool, false)
    default_vpc_name               = string
    ebs_csi_kms_cmk_ids            = optional(list(string))
    manage_aws_auth_configmap      = optional(bool, false)
    prometheus_security_group_id   = optional(string)
    subnet_ids                     = list(string)
  })
}

variable "eks" {
  type = object({
    cluster_name = optional(string, "eks-cluster")
    cluster_security_group_additional_rules = optional(list(object({
      name                     = string
      description              = string
      protocol                 = string
      from_port                = number
      to_port                  = number
      type                     = string
      source_security_group_id = string
    })), [])
    cluster_version = optional(string)
    aws_auth_roles = optional(list(
      object({
        rolearn  = string
        username = string
        groups   = list(string)
    })))
    cluster_enabled_log_types      = optional(list(string))
    cluster_endpoint_public_access = optional(bool)
    default_vpc_name               = optional(string)
    ebs_csi_kms_cmk_ids            = optional(list(string))
    manage_aws_auth_configmap      = optional(bool)
    managed_node_groups = list(object({
      name                 = string
      instance_types       = list(string)
      bootstrap_extra_args = optional(string)
      desired_size         = number
      disk_size            = optional(number, 100)
      ebs_optimized        = optional(bool, true)
      max_size             = number
      min_size             = number
      remote_access = optional(object({
        ec2_ssh_key               = optional(string)
        source_security_group_ids = optional(list(string))
      }))
      taints = optional(map(object({
        key    = string
        value  = string
        effect = string
      })), {})
      labels                     = optional(map(string), {})
      pre_bootstrap_user_data    = optional(string)
      use_custom_launch_template = optional(bool, false)
      })
    )
    node_security_group_additional_rules = optional(list(object({
      name                     = string
      description              = string
      protocol                 = string
      from_port                = number
      to_port                  = number
      type                     = string
      source_security_group_id = string
    })), [])
    prometheus_security_group_id = optional(string)
    subnet_ids                   = optional(list(string))
  })
}
