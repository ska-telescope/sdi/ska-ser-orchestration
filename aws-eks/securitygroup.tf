data "aws_subnet" "target" {
  for_each = toset(local.subnet_ids)
  id       = each.value
}

data "aws_security_group" "control_plane_eni_sg" {
  filter {
    name   = "group-name"
    values = ["eks-cluster-sg-${local.cluster_name}*"]
  }
}

# This is needed as Amazon add an SG to the control plane ENI
# that is not exposed to the EKS terraform module to modify
resource "aws_security_group_rule" "expose_kubelet_metrics" {
  type                     = "ingress"
  description              = "Kubelet from CICD Monitoring"
  protocol                 = "tcp"
  from_port                = 10250
  to_port                  = 10250
  source_security_group_id = local.prometheus_security_group_id
  security_group_id        = data.aws_security_group.control_plane_eni_sg.id
}

resource "aws_security_group" "allow_nfs" {
  name        = "allow nfs for ${local.cluster_name}"
  description = "Allow NFS inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "NFS from VPC"
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
    cidr_blocks = values(data.aws_subnet.target)[*].cidr_block
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}


