resource "aws_efs_file_system" "node_efs" {
  creation_token = "efs-for-${local.cluster_name}-nodes"
}

resource "aws_efs_mount_target" "efs-mt" {
  for_each        = toset(local.subnet_ids)
  file_system_id  = aws_efs_file_system.node_efs.id
  subnet_id       = each.value
  security_groups = [aws_security_group.allow_nfs.id]
}