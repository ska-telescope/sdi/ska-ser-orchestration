
locals {
  aws_auth_roles                 = flatten(coalescelist(var.eks.aws_auth_roles, var.defaults.aws_auth_roles))
  cluster_endpoint_public_access = coalesce(var.eks.cluster_endpoint_public_access, var.defaults.cluster_endpoint_public_access)
  cluster_name                   = coalesce(var.eks.cluster_name, var.defaults.cluster_name)
  cluster_security_group_additional_rules = {
    for group in lookup(var.eks, "cluster_security_group_additional_rules", []) :
    group["name"] => {
      description              = group["description"]
      protocol                 = group["protocol"]
      from_port                = group["from_port"]
      to_port                  = group["to_port"]
      type                     = group["type"]
      source_security_group_id = group["source_security_group_id"]
    }
  }
  cluster_version           = try(coalesce(var.eks.cluster_version, var.defaults.cluster_version), null)
  default_vpc_name          = coalesce(var.eks.default_vpc_name, var.defaults.default_vpc_name)
  ebs_csi_kms_cmk_ids       = try(coalescelist(var.eks.ebs_csi_kms_cmk_ids, var.defaults.ebs_csi_kms_cmk_ids), null)
  cluster_enabled_log_types = coalesce(var.eks.cluster_enabled_log_types, var.defaults.cluster_enabled_log_types)
  node_security_group_additional_rules = {
    for group in lookup(var.eks, "node_security_group_additional_rules", []) :
    group["name"] => {
      description              = group["description"]
      protocol                 = group["protocol"]
      from_port                = group["from_port"]
      to_port                  = group["to_port"]
      type                     = group["type"]
      source_security_group_id = group["source_security_group_id"]
    }
  }
  managed_node_groups = {
    for group in lookup(var.eks, "managed_node_groups", []) :
    (join("_", [group["name"], "node_group"])) => {
      name                       = join("-", [local.cluster_name, group["name"]])
      instance_types             = group["instance_types"]
      disk_size                  = group["disk_size"]
      desired_size               = group["desired_size"]
      max_size                   = group["max_size"]
      min_size                   = group["min_size"]
      remote_access              = group["remote_access"]
      taints                     = group["taints"]
      labels                     = group["labels"]
      ebs_optimized              = group["ebs_optimized"]
      use_custom_launch_template = group["use_custom_launch_template"]
    }
  }
  manage_aws_auth_configmap    = coalesce(var.eks.manage_aws_auth_configmap, var.defaults.manage_aws_auth_configmap)
  prometheus_security_group_id = coalesce(var.eks.prometheus_security_group_id, var.defaults.prometheus_security_group_id)
  subnet_ids                   = coalescelist(var.eks.subnet_ids, var.defaults.subnet_ids)
}

data "aws_vpc" "default" {
  filter {
    name   = "tag:Name"
    values = [local.default_vpc_name]
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "20.20.0"


  cluster_name                            = local.cluster_name
  cluster_security_group_additional_rules = local.cluster_security_group_additional_rules
  cluster_version                         = local.cluster_version
  vpc_id                                  = data.aws_vpc.default.id
  subnet_ids                              = local.subnet_ids
  cluster_enabled_log_types               = local.cluster_enabled_log_types
  cluster_endpoint_public_access          = local.cluster_endpoint_public_access
  eks_managed_node_groups                 = local.managed_node_groups
  node_security_group_additional_rules    = local.node_security_group_additional_rules

  cluster_addons = {
    aws-ebs-csi-driver = {
      service_account_role_arn = module.ebs_csi_irsa_role.iam_role_arn
      most_recent              = true
    },
    aws-efs-csi-driver = {
      service_account_role_arn = module.efs_csi_irsa_role.iam_role_arn
      most_recent              = true
    }
  }
}

# AWS auth config
module "eks_aws_auth" {
  source  = "terraform-aws-modules/eks/aws//modules/aws-auth"
  version = "20.20.0"

  # aws-auth configmap
  manage_aws_auth_configmap = local.manage_aws_auth_configmap
  aws_auth_roles            = local.aws_auth_roles
}

# EBS CSI IAM Role
module "ebs_csi_irsa_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "5.41.0"

  role_name             = "${local.cluster_name}-ebs-csi"
  attach_ebs_csi_policy = true
  ebs_csi_kms_cmk_ids   = local.ebs_csi_kms_cmk_ids # Add KMS keys to allow encrypted EBS vols

  oidc_providers = {
    ex = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:ebs-csi-controller-sa"]
    }
  }
}

# EFS CSI IAM Role
module "efs_csi_irsa_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
  version = "5.41.0"

  role_name             = "${local.cluster_name}-efs-csi"
  attach_efs_csi_policy = true

  oidc_providers = {
    ex = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:efs-csi-controller-sa"]
    }
  }
}