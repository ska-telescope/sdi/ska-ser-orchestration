variable "defaults" {
  type = object({
    namespace           = optional(string)
    name                = optional(string)
    allowed_cidr_blocks = optional(list(string))
    cluster_family      = optional(string)
    cluster_parameters = optional(list(object({
      apply_method = string
      name         = string
      value        = string
    })), [])
    cluster_size            = number
    deletion_protection     = optional(bool, true)
    engine_version          = optional(string)
    instance_class          = string
    default_vpc_name        = string
    subnet_ids              = list(string)
    allowed_security_groups = optional(list(string))
  })
}

variable "documentdb" {
  type = object({
    namespace           = optional(string)
    name                = optional(string)
    allowed_cidr_blocks = optional(list(string))
    cluster_family      = optional(string)
    cluster_parameters = optional(list(object({
      apply_method = string
      name         = string
      value        = string
    })), [])
    cluster_size            = optional(number)
    deletion_protection     = optional(bool)
    engine_version          = optional(string)
    master_username         = optional(string)
    master_password         = optional(string)
    instance_class          = optional(string)
    default_vpc_name        = optional(string)
    subnet_ids              = optional(list(string))
    allowed_security_groups = optional(list(string))
  })
}

