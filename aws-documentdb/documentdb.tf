
locals {
  namespace               = try(coalesce(var.documentdb.namespace, var.defaults.namespace), null)
  stage                   = try(coalesce(var.documentdb.stage, var.defaults.stage), null)
  name                    = coalesce(var.documentdb.name, var.defaults.name)
  allowed_cidr_blocks     = try(coalescelist(var.documentdb.allowed_cidr_blocks, var.defaults.allowed_cidr_blocks), [])
  cluster_family          = try(coalesce(var.documentdb.cluster_family, var.defaults.cluster_family), null)
  cluster_parameters      = try(coalescelist(var.documentdb.cluster_parameters, var.defaults.cluster_parameters), [])
  cluster_size            = coalesce(var.documentdb.cluster_size, var.defaults.cluster_size)
  deletion_protection     = coalesce(var.documentdb.deletion_protection, var.defaults.deletion_protection)
  engine_version          = try(coalesce(var.documentdb.engine_version, var.defaults.engine_version), null)
  master_username         = var.documentdb.master_username
  master_password         = var.documentdb.master_password
  instance_class          = coalesce(var.documentdb.instance_class, var.defaults.instance_class)
  default_vpc_name        = coalesce(var.documentdb.default_vpc_name, var.defaults.default_vpc_name)
  subnet_ids              = coalescelist(var.documentdb.subnet_ids, var.defaults.subnet_ids)
  allowed_security_groups = try(coalescelist(var.documentdb.allowed_security_groups, var.defaults.allowed_security_groups), [])
}

data "aws_vpc" "default" {
  filter {
    name   = "tag:Name"
    values = [local.default_vpc_name]
  }
}

module "documentdb_cluster" {
  source  = "cloudposse/documentdb-cluster/aws"
  version = "0.23.0"

  namespace               = local.namespace
  stage                   = local.stage
  name                    = local.name
  allowed_cidr_blocks     = local.allowed_cidr_blocks
  cluster_family          = local.cluster_family
  cluster_parameters      = local.cluster_parameters
  cluster_size            = local.cluster_size
  deletion_protection     = local.deletion_protection
  engine_version          = local.engine_version
  master_username         = local.master_username
  master_password         = local.master_password
  instance_class          = local.instance_class
  vpc_id                  = data.aws_vpc.default.id
  subnet_ids              = local.subnet_ids
  allowed_security_groups = local.allowed_security_groups
}