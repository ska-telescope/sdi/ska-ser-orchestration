#!/usr/bin/env -S python3 -u
"""This script converts a Terraform state file (pulled from GitLab) into
an ansible-inventory file, in YAML format.
"""

import base64
import json
import logging
import os
import pathlib
import re
import sys
from argparse import ArgumentParser

import requests
import yaml


def str2bool(string: str):
    """
    Convert a boolean string representation to boolean
    """
    return string.strip().lower() in ("true", "1", "yes")


# Keys
SSH_AWS_SSM_SCRIPT_PATH = os.environ.get(
    "SSH_AWS_SSM_SCRIPT_PATH", "../resources/scripts/ssh-aws-ssm.sh"
)
SSH_KEY_LOCATION = os.environ.get("SSH_KEY_LOCATION", "../resources/keys")
LEGACY_INVENTORY = str2bool(os.environ.get("LEGACY_INVENTORY", "true"))
SSH_KEY_LOCATIONS = [
    "../resources/keys",
    "./keys",
    "~/.ssh",
    ".",
    "..",
]
GRAPHQL_URL = "https://gitlab.com/api/graphql"
LOGGING_LEVEL = os.environ.get("LOGGING_LEVEL", "INFO")
LOGGING_FORMAT = (
    "%(asctime)s [level=%(levelname)s] "
    "[module=%(module)s] [line=%(lineno)d]: %(message)s"
)
logging.basicConfig(level=LOGGING_LEVEL, format=LOGGING_FORMAT)
log = logging.getLogger("tfstate_to_ansible_inventory")
log.debug("Logging level is: %s", LOGGING_LEVEL)

parser = ArgumentParser(
    description="Terraform State to Ansible Inventory Conversion Tool"
)
parser.add_argument(
    "-o",
    dest="output",
    required=False,
    default="inventory",
    help="output directory",
)
parser.add_argument(
    "-f",
    dest="file_name",
    required=False,
    default="inventory.yml",
    help="output file name",
)
parser.add_argument(
    "-i",
    dest="local_state",
    required=False,
    default=None,
    help="local state file",
)
parser.add_argument(
    "-e",
    dest="environment",
    required=False,
    default=None,
    help="target environment",
)
parser.add_argument(
    "-d",
    dest="datacentre",
    required=False,
    default=None,
    help="target datacentre",
)
parser.add_argument(
    "-s",
    dest="service",
    required=False,
    default=None,
    help="target service",
)
parser.add_argument(
    "--no-jumphost",
    dest="no_jumphost",
    default=False,
    action="store_true",
    help="disable jumphost (connection through VPN)",
)
parser.add_argument(
    "--prefer-floating-ip",
    dest="prefer_floating_ip",
    default=False,
    action="store_true",
    help="prefer floating ips to local ips",
)
parser.add_argument(
    "--downstream-precedence",
    dest="downstream_precedence",
    default=False,
    action="store_true",
    help="give precedence to downstream inventories",
)

args = parser.parse_args()
if args.no_jumphost:
    log.info("** ---------- Jumphost DISABLED ---------- **")
    log.info("** Make sure you connect to a suitable VPN **")
    log.info("** ---------- Jumphost DISABLED ---------- **")


def get_env(variables, default=None):
    """
    Returns the first defined environment variable value for a set of
    variables. If none of the variables are found, returns a default.
    """
    for k in variables:
        value = os.environ.get(k, None)
        if value is not None:
            return value

    return default


def get_ssh_key(keypair: str):
    """
    Gets the full path of an SSH string
    """
    return f"{os.path.join(SSH_KEY_LOCATION, keypair)}.pem"


def is_aws_instance(source_inventory: dict):
    """
    Tells if an inventory relates to an AWS instance or not
    """
    return "region" in source_inventory and source_inventory.get(
        "ansible_host", ""
    ).startswith("i-")


def create_inline_ssh_config(
    source_inventory,
    source_inventory_type,
    jumphost_enabled,
    prefer_floating_ip,
):
    """
    Creates the inventory with the SSH configuration inline. This is required
    to use funcional static (comitted) inventories.

    LEGACY_INVENTORY will make it so that the ansible inventory file is
    complemented by an auto-generated ssh.config file, containing the actual
    ssh configurations. With the new format, all of the SSH configurations will
    be placed in line (using ansible_ssh_extra_args variable) and no dynamic
    ssh.config file will need to be generated.
    """
    if LEGACY_INVENTORY:
        return source_inventory

    if source_inventory_type in ["cluster", "instance_group"]:
        if "children" in source_inventory:
            for group_id, group_config in source_inventory["children"].items():
                source_inventory["children"][
                    group_id
                ] = create_inline_ssh_config(
                    group_config,
                    "instance_group",
                    jumphost_enabled,
                    prefer_floating_ip,
                )

        if "hosts" in source_inventory:
            for host_id, host_config in source_inventory["hosts"].items():
                source_inventory["hosts"][host_id] = create_inline_ssh_config(
                    host_config,
                    "instance",
                    jumphost_enabled,
                    prefer_floating_ip,
                )
    else:
        if is_aws_instance(source_inventory):
            region = source_inventory["region"]
            keypair = get_ssh_key(source_inventory["keypair"])
            source_inventory[
                "ansible_ssh_extra_args"
            ] = f'-o ProxyCommand="{SSH_AWS_SSM_SCRIPT_PATH} \
%h %p {region}" -i {keypair}'
        else:
            keypair = get_ssh_key(source_inventory["keypair"])
            target_instance_ip = source_inventory["ip"]
            if prefer_floating_ip and source_inventory.get(
                "floating_ip", None
            ):
                target_instance_ip = source_inventory["floating_ip"]

            source_inventory["ansible_host"] = source_inventory[
                "ansible_host"
            ] = target_instance_ip
            source_inventory["ansible_ssh_extra_args"] = f'-i "{keypair}"'
            if source_inventory.get("jump_host", None) and jumphost_enabled:
                config_jumphost(source_inventory, prefer_floating_ip, keypair)

    return source_inventory


def config_jumphost(source_inventory, prefer_floating_ip, instance_keypair):
    """
    Configures an inventory host to use a jumphost when connecting over
    SSH
    """
    jump_host_ip = source_inventory["jump_host"]["ip"]
    jumphost_keypair = get_ssh_key(source_inventory["jump_host"]["keypair"])
    if prefer_floating_ip and source_inventory["jump_host"].get(
        "floating_ip", None
    ):
        jump_host_ip = source_inventory["jump_host"]["floating_ip"]

    jumphost_ssh = f'{source_inventory["jump_host"]["user"]}@{jump_host_ip}'
    source_inventory[
        "ansible_ssh_extra_args"
    ] = f'-o ProxyCommand="ssh -W \
%h:%p -i {jumphost_keypair} {jumphost_ssh}" -i {instance_keypair}'


def get_inventory(
    this_tf_state, inventory_type, jumphost_enabled, prefer_floating_ip
):
    """
    Gets all the inventories of a given type
    """
    result = {}
    for resource in this_tf_state:
        if (
            resource["mode"] == "managed"
            and resource["type"] == "null_resource"
            and resource["name"] == "inventory"
        ):
            if "instances" in resource and len(resource["instances"]) > 0:
                inventory_info = resource["instances"][0]["attributes"][
                    "triggers"
                ]
                inventories = json.loads(
                    base64.b64decode(inventory_info["inventory"]).decode(
                        "utf-8"
                    )
                )
                if inventory_info["type"] == inventory_type:
                    for (inventory_id, inventory_value) in inventories.items():
                        result[inventory_id] = create_inline_ssh_config(
                            inventory_value,
                            inventory_type,
                            jumphost_enabled,
                            prefer_floating_ip,
                        )
            else:
                log.error("Failed to retrieve inventory for '%s'", resource)

    return result


def get_ssh_config(host_name, user, host_ip, keypair, jump_host=None):
    """
    Creates an SSH host entry for an host, to be added to an ssh
    config file
    """
    identities = "\n".join(
        [
            f"    IdentityFile {location}/{keypair}.pem"
            for location in SSH_KEY_LOCATIONS
        ]
    )

    return f"""
Host {host_name}
    Hostname {host_ip}
    User {user}
{identities}
    {f"ProxyJump {jump_host}" if jump_host is not None else ""}
"""


# Create output directory
output = os.path.abspath(args.output)
pathlib.Path(output).mkdir(parents=True, exist_ok=True)

if args.local_state is None:
    # Get state using HTTP
    url = get_env(["TF_STATE_ADDRESS", "TF_HTTP_ADDRESS"])
    username = get_env(["TF_STATE_USERNAME", "TF_HTTP_USERNAME"])
    password = get_env(["TF_STATE_PASSWORD", "TF_HTTP_PASSWORD"])

    # TO DO: switch to using full path name instead of project id
    project_id = get_env(["GITLAB_PROJECT_ID", "TF_HTTP_PASSWORD"])
    STATE_BASE_URL = (
        f"https://gitlab.com/api/v4/projects/{project_id}/terraform/state/"
    )

    # common headers for API calls
    headers = {
        "Content-type": "application/json",
        "Accept": "*/*",
        "PRIVATE-TOKEN": password,
        "Authorization": f"Bearer {password}",
    }

    # get the full path name for the project id
    api_query = f"https://gitlab.com/api/v4/projects/{project_id}/"
    log.debug("Project URI: %s", api_query)
    try:
        project_request = requests.get(api_query, headers=headers, timeout=60)
        if project_request.status_code != 200:
            content = json.dumps(project_request.json(), indent=4)
            log.critical(
                "** ERROR get project [%s]:\n%s",
                project_request.status_code,
                content,
            )
            sys.exit(project_request.status_code)
    except requests.exceptions.RequestException as err:
        log.critical("** error getting project path_with_namespace: %s", err)
        sys.exit(-1)

    # get list of tfstates using graphql API
    # select by path: project(fullPath:
    #                 "ska-telescope/sdi/ska-ser-infra-machinery")
    try:
        path_with_namespace = project_request.json().get(
            "path_with_namespace", ""
        )
        log.debug(
            "Project path_with_namespace %s:%s",
            project_id,
            path_with_namespace,
        )
        gql_query = {
            "query": (
                "query { project("
                f'fullPath: "{path_with_namespace}") '
                " { name id terraformStates { nodes {name}}}}"
            )
        }

        tf_all_states_request = requests.post(
            GRAPHQL_URL, json=gql_query, headers=headers, timeout=60
        )
        if tf_all_states_request.status_code != 200:
            log.critical(
                "** ERROR get tfstates [%s]:\n%s",
                tf_all_states_request.status_code,
                tf_all_states_request.content(),
            )
            sys.exit(tf_all_states_request.status_code)
    except requests.exceptions.RequestException as err:
        log.critical("** error getting tfstates: %s", err)
        sys.exit(-1)

    # extract the tfstates
    states = tf_all_states_request.json().get("data", {})
else:
    states = {
        "project": {
            "terraformStates": {
                "nodes": ["local"],
            }
        }
    }

# Parse groupings
instance_groups = {}
instance_groups_in_clusters = []
instances_with_parent = []
total_cluster_inventories = {}
total_instance_group_inventories = {}
total_instance_inventories = {}

# iterate over tfstate names to get the actual states
for state in states["project"]["terraformStates"]["nodes"]:
    if state != "local":
        PREFIX = "-".join(
            list(
                filter(None, [args.datacentre, args.environment, args.service])
            )
        )
        if not state["name"].startswith(PREFIX):
            continue

        log.info("Getting state from %s%s", STATE_BASE_URL, state["name"])
        try:
            tf_state_request = requests.get(
                STATE_BASE_URL + state["name"],
                auth=(username, password),
                timeout=60,
            )
            if tf_state_request.status_code != 200:
                if tf_state_request.status_code == 204:
                    # no content
                    continue

                log.critical(
                    "** ERROR [%s]:\n%s",
                    tf_state_request.status_code,
                    tf_state_request.content.decode("utf-8"),
                )
                sys.exit(tf_state_request.status_code)
        except requests.exceptions.RequestException as err:
            log.critical(
                "** ERROR [%s] getting tfstate: %s", state["name"], err
            )
            sys.exit(-1)

        tf_state = tf_state_request.json().get("resources", [])

    else:
        with open(args.local_state, encoding="utf-8") as f:
            tf_state_json: dict = json.load(f)
            tf_state = tf_state_json.get("resources", [])

    # Get class inventories
    cluster_inventories = get_inventory(
        tf_state, "cluster", not args.no_jumphost, args.prefer_floating_ip
    )
    instance_group_inventories = get_inventory(
        tf_state,
        "instance_group",
        not args.no_jumphost,
        args.prefer_floating_ip,
    )
    instance_inventories = get_inventory(
        tf_state, "instance", not args.no_jumphost, args.prefer_floating_ip
    )

    for (cluster_id, cluster) in cluster_inventories.items():
        for instance_group_id in cluster["children"]:
            instance_groups_in_clusters.append(instance_group_id)
        for instance_id in cluster["hosts"]:
            instances_with_parent.append(instance_id)

    for (
        instance_group_id,
        instance_group,
    ) in instance_group_inventories.items():
        instance_groups[instance_group_id] = {"hosts": instance_group["hosts"]}
        for instance_id in instance_group["hosts"]:
            instances_with_parent.append(instance_id)
    total_cluster_inventories = {
        **total_cluster_inventories,
        **cluster_inventories,
    }
    total_instance_group_inventories = {
        **total_instance_group_inventories,
        **instance_group_inventories,
    }
    total_instance_inventories = {
        **total_instance_inventories,
        **instance_inventories,
    }

# Update instance inventories in their groups
# This is needed for us to pull changes done to the instance inventory
# but not to the upstream groups
if args.downstream_precedence:
    for (
        instance_group_name,
        instance_group,
    ) in total_instance_group_inventories.items():
        for (
            instance_name,
            instance_inventory,
        ) in instance_group["hosts"].items():
            downstream_instance_inventory = total_instance_inventories.get(
                instance_name, instance_inventory
            )
            if (
                downstream_instance_inventory is not None
                and downstream_instance_inventory != instance_inventory
            ):
                log.warning(
                    "Downstream inventory differs for '%s' at '%s'",
                    instance_name,
                    instance_group_name,
                )
                instance_group["hosts"][
                    instance_name
                ] = downstream_instance_inventory

    # Try to assign loose instances to groups of the same name
    for instance_name in total_instance_inventories:
        if instance_name not in instances_with_parent:
            for (
                instance_group_name,
                instance_group,
            ) in total_instance_group_inventories.items():
                if re.match(instance_group_name + r"-i[0-9]+", instance_name):
                    log.warning(
                        "Assigning loose instance '%s' to '%s' ...",
                        instance_name,
                        instance_group_name,
                    )
                    instance_group["hosts"][instance_name] = None

# Create inventory
inventory = {
    **{  # all with groups and single hosts
        "all": {
            "children": {
                **{cluster: None for cluster in total_cluster_inventories},
                **{
                    instance_group: None
                    for instance_group in total_instance_group_inventories
                    if instance_group not in instance_groups_in_clusters
                },
            },
            "hosts": {
                instance_id: instance
                for (
                    instance_id,
                    instance,
                ) in total_instance_inventories.items()
                if instance_id not in instances_with_parent
            },
        }
    },
    **{  # cluster with instance groups and hosts
        cluster_id: {
            "children": {
                instance_group_id: None
                for instance_group_id in cluster["children"]
            },
            "hosts": cluster["hosts"],
        }
        for (cluster_id, cluster) in total_cluster_inventories.items()
    },
    **{  # instance groups
        instance_group_id: {"hosts": instance_group["hosts"]}
        for (
            instance_group_id,
            instance_group,
        ) in total_instance_group_inventories.items()
    },
}

# Output inventory
inventory_path = os.path.join(output, args.file_name)
with open(inventory_path, "w+", encoding="utf-8") as f:
    yaml.safe_dump(inventory, indent=2, stream=f, width=256)
    log.info("Inventory at %s", inventory_path)

if LEGACY_INVENTORY:
    # Create ssh config
    ssh_config = [
        "BatchMode yes",
        "StrictHostKeyChecking no",
        "LogLevel QUIET",
    ]

    # Add jump hosts
    jump_hosts = {}
    for (instance_id, instance) in total_instance_inventories.items():
        if instance["jump_host"] is not None:
            jump_hosts[instance["jump_host"]["hostname"]] = instance[
                "jump_host"
            ]

        instance_ip = instance["ip"]
        if (
            args.no_jumphost
            and args.prefer_floating_ip
            and instance["floating_ip"] not in [None, ""]
        ):
            instance_ip = instance["floating_ip"]

        ssh_config.append(
            get_ssh_config(
                host_name=instance_id,
                user=instance["ansible_user"],
                host_ip=instance_ip,
                keypair=instance["keypair"],
                jump_host=None
                if (args.no_jumphost or instance["jump_host"] is None)
                else instance["jump_host"]["hostname"],
            )
        )

    for (host, config) in jump_hosts.items():
        jumphost_ip = config["ip"]
        if args.prefer_floating_ip and config["floating_ip"] not in [None, ""]:
            jumphost_ip = config["floating_ip"]

        ssh_config.append(
            get_ssh_config(
                host_name=host,
                user=config["user"],
                host_ip=jumphost_ip,
                keypair=config["keypair"],
            )
        )

    # Output ssh config
    ssh_config_path = os.path.join(output, "ssh.config")
    with open(ssh_config_path, "w+", encoding="utf-8") as f:
        f.write("\n".join(ssh_config))
        log.info("SSH Config at %s", ssh_config_path)
