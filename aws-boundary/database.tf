locals {
  database = {
    cluster_identifier   = coalesce(var.boundary.database.cluster_identifier, var.defaults.database.cluster_identifier)
    allocated_storage    = coalesce(var.boundary.database.allocated_storage, var.defaults.database.allocated_storage)
    database_name        = coalesce(var.boundary.database.database_name, var.defaults.database.database_name)
    db_subnet_group_name = coalesce(var.boundary.database.db_subnet_group_name, var.defaults.database.db_subnet_group_name)
    db_subnets           = coalescelist(var.boundary.database.db_subnets, var.defaults.database.db_subnets)
    engine               = coalesce(var.boundary.database.engine, var.defaults.database.engine)
    engine_version       = coalesce(var.boundary.database.engine_version, var.defaults.database.engine_version)
    db_instance_type     = coalesce(var.boundary.database.db_instance_type, var.defaults.database.db_instance_type)
    storage_type         = coalesce(var.boundary.database.storage_type, var.defaults.database.storage_type)
    master_username      = coalesce(var.boundary.database.master_username, var.defaults.database.master_username)
    master_password      = coalesce(var.boundary.database.master_password, var.defaults.database.master_password)
    skip_final_snapshot  = coalesce(var.boundary.database.skip_final_snapshot, var.defaults.database.skip_final_snapshot)
    instance_count       = coalesce(var.boundary.database.instance_count, var.defaults.database.instance_count)
  }
}

resource "aws_db_subnet_group" "data-subnet-group" {
  name       = "main"
  subnet_ids = local.database.db_subnets
}

resource "aws_rds_cluster" "db_cluster" {
  cluster_identifier     = local.database.cluster_identifier
  database_name          = local.database.database_name
  db_subnet_group_name   = local.database.db_subnet_group_name
  engine                 = local.database.engine
  engine_version         = local.database.engine_version
  master_username        = local.database.master_username
  master_password        = local.database.master_password
  skip_final_snapshot    = local.database.skip_final_snapshot
  storage_type           = local.database.storage_type
  vpc_security_group_ids = [aws_security_group.rds.id]
}

resource "aws_rds_cluster_instance" "cluster_instances" {
  count                = local.database.instance_count
  identifier_prefix    = "${local.database.cluster_identifier}-"
  cluster_identifier   = aws_rds_cluster.db_cluster.id
  instance_class       = local.database.db_instance_type
  engine               = local.database.engine
  engine_version       = local.database.engine_version
  db_subnet_group_name = local.database.db_subnet_group_name

  depends_on = [aws_db_subnet_group.data-subnet-group, aws_rds_cluster.db_cluster]
}