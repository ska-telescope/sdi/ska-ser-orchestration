variable "defaults" {
  type = object({
    default_vpc_name = string
    controller = object({
      name                 = string
      availability_zone    = string
      instance_type        = string
      iam_instance_profile = optional(string)
      ami                  = string
      keypair              = string
      subnet_id            = string
      jump_host            = optional(string)
      vpn_cidr_blocks      = optional(list(string))
    })
    worker = object({
      name                 = string
      availability_zone    = string
      instance_type        = string
      iam_instance_profile = optional(string)
      ami                  = string
      keypair              = string
      subnet_id            = string
      jump_host            = optional(string)
      vpn_cidr_blocks      = optional(list(string))
    })
    database = object({
      cluster_identifier   = string
      database_name        = string
      db_subnet_group_name = optional(string)
      db_subnets           = optional(list(string))
      allocated_storage    = string
      db_instance_type     = string
      storage_type         = string
      engine               = string
      engine_version       = string
      master_username      = string
      master_password      = string
      skip_final_snapshot  = optional(bool, true)
      instance_count       = string
    })
    kms = object({
      root = object({
        name                = string
        master_key_spec     = string
        is_enabled          = optional(bool, true)
        enable_key_rotation = optional(bool, true)
      })
      worker = object({
        name                = string
        master_key_spec     = string
        is_enabled          = optional(bool, true)
        enable_key_rotation = optional(bool, true)
      })
      recovery = object({
        name                = string
        master_key_spec     = string
        is_enabled          = optional(bool, true)
        enable_key_rotation = optional(bool, true)
      })
    })
    loadbalancer = object({
      external = optional(object({
        name                       = string
        certificate_arn            = optional(string)
        environment                = optional(string)
        internal                   = bool
        load_balancer_type         = string
        security_groups            = optional(list(string))
        subnets                    = list(string)
        enable_deletion_protection = optional(bool, true)
      }))
      internal = optional(object({
        name                       = string
        certificate_arn            = optional(string)
        environment                = optional(string)
        internal                   = bool
        load_balancer_type         = string
        security_groups            = optional(list(string))
        subnets                    = list(string)
        enable_deletion_protection = optional(bool, true)
      }))
    })
  })
}

variable "boundary" {
  type = object({
    name             = optional(string, "boundary")
    default_vpc_name = optional(string)
    controller = optional(object({
      name                 = optional(string, "controller")
      instance_type        = optional(string)
      iam_instance_profile = optional(string)
      ami                  = optional(string)
      availability_zone    = optional(string)
      subnet_id            = optional(string)
      keypair              = optional(string)
      jump_host            = optional(string)
      roles                = optional(list(string), ["controller"])
    }))
    worker = optional(object({
      name                 = optional(string, "controller")
      instance_type        = optional(string)
      iam_instance_profile = optional(string)
      ami                  = optional(string)
      availability_zone    = optional(string)
      subnet_id            = optional(string)
      keypair              = optional(string)
      jump_host            = optional(string)
      roles                = optional(list(string), ["controller"])
    }))
    database = optional(object({
      cluster_identifier   = optional(string)
      database_name        = optional(string, "boundary")
      db_subnet_group_name = optional(string)
      db_subnets           = optional(list(string), [])
      allocated_storage    = optional(string)
      db_instance_type     = optional(string)
      storage_type         = optional(string)
      engine               = optional(string)
      engine_version       = optional(string)
      master_username      = optional(string)
      master_password      = optional(string)
      skip_final_snapshot  = optional(bool, true)
      security_groups      = optional(list(string), [])
      instance_count       = optional(string)
    }))
    kms = optional(object({
      root = optional(object({
        name                = optional(string)
        master_key_spec     = optional(string)
        is_enabled          = optional(bool)
        enable_key_rotation = optional(bool)
      }))
      worker = optional(object({
        name                = optional(string)
        master_key_spec     = optional(string)
        is_enabled          = optional(bool)
        enable_key_rotation = optional(bool)
      }))
      recovery = optional(object({
        name                = optional(string)
        master_key_spec     = optional(string)
        is_enabled          = optional(bool)
        enable_key_rotation = optional(bool)
      }))
    }))
    loadbalancer = optional(object({
      external = optional(object({
        name                       = optional(string)
        certificate_arn            = optional(string)
        environment                = optional(string)
        internal                   = optional(bool)
        load_balancer_type         = optional(string)
        security_groups            = optional(list(string))
        subnets                    = optional(list(string))
        enable_deletion_protection = optional(bool)
      }))
      internal = optional(object({
        name                       = optional(string)
        certificate_arn            = optional(string)
        environment                = optional(string)
        internal                   = optional(bool)
        load_balancer_type         = optional(string)
        security_groups            = optional(list(string))
        subnets                    = optional(list(string))
        enable_deletion_protection = optional(bool)
      }))
    }))
  })
}

