locals {
  default_vpc_name = coalesce(var.boundary.default_vpc_name, var.defaults.default_vpc_name)
  securitygroup = {
    controller_subnet_id = coalesce(var.boundary.controller.subnet_id, var.defaults.controller.subnet_id)
    worker_subnet_id     = coalesce(var.boundary.worker.subnet_id, var.defaults.worker.subnet_id)
  }
}
data "aws_subnet" "controllers" {
  id = local.securitygroup.controller_subnet_id
}

data "aws_subnet" "workers" {
  id = local.securitygroup.worker_subnet_id
}

data "aws_vpc" "default" {
  filter {
    name   = "tag:Name"
    values = [local.default_vpc_name]
  }
}

resource "aws_security_group" "rds" {
  name        = "boundary_rds"
  description = "Boundary RDS SG"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "Postgres from Boundary Controllers"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [data.aws_subnet.controllers.cidr_block]
  }

  tags = {
    Name = "boundary_rds"
  }

}

resource "aws_security_group" "controllers" {
  name        = "boundary_controllers"
  description = "Boundary Controllers SG"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "Boundary traffic from Loadbalancers"
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    security_groups = [aws_security_group.external_loadbalancer.id,
    aws_security_group.internal_loadbalancer.id]
  }

  ingress {
    description = "SSH Traffic from Default VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.default.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "boundary_controllers"
  }

  depends_on = [aws_lb.loadbalancer_external,
  aws_lb.loadbalancer_internal]
}

resource "aws_security_group" "workers" {
  name        = "boundary_workers"
  description = "Boundary Workers SG"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "Traffic from Boundary Controllers"
    from_port   = 9202
    to_port     = 9202
    protocol    = "tcp"
    cidr_blocks = [data.aws_subnet.workers.cidr_block]
  }

  tags = {
    Name = "boundary_workers"
  }
}

resource "aws_security_group" "external_loadbalancer" {
  name        = "boundary_external_loadbalancer"
  description = "Boundary External Loadbalancer SG"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "HTTPS from External"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "boundary_external_loadbalancer"
  }
}

resource "aws_security_group" "internal_loadbalancer" {
  name        = "boundary_internal_loadbalancer"
  description = "Boundary Internal Loadbalancer SG"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description = "Boundary Controller internal traffic from VPC"
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "boundary_internal_loadbalancer"
  }
}