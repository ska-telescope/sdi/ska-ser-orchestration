locals {
  loadbalancer = {
    external = {
      name                       = coalesce(var.boundary.loadbalancer.external.name, var.defaults.loadbalancer.external.name)
      certificate_arn            = try(coalesce(var.boundary.loadbalancer.external.certificate_arn, var.defaults.loadbalancer.external.certificate_arn), null)
      environment                = coalesce(var.boundary.loadbalancer.external.environment, var.defaults.loadbalancer.external.environment)
      internal                   = coalesce(var.boundary.loadbalancer.external.internal, var.defaults.loadbalancer.external.internal)
      load_balancer_type         = coalesce(var.boundary.loadbalancer.external.load_balancer_type, var.defaults.loadbalancer.external.load_balancer_type)
      security_groups            = try(coalescelist(var.boundary.loadbalancer.external.security_groups, var.defaults.loadbalancer.external.security_groups), [])
      subnets                    = coalescelist(var.boundary.loadbalancer.external.subnets, var.defaults.loadbalancer.external.subnets)
      enable_deletion_protection = coalesce(var.boundary.loadbalancer.external.enable_deletion_protection, var.defaults.loadbalancer.external.enable_deletion_protection)
    }
    internal = {
      name                       = coalesce(var.boundary.loadbalancer.internal.name, var.defaults.loadbalancer.internal.name)
      certificate_arn            = try(coalesce(var.boundary.loadbalancer.internal.certificate_arn, var.defaults.loadbalancer.internal.certificate_arn), null)
      environment                = coalesce(var.boundary.loadbalancer.internal.environment, var.defaults.loadbalancer.internal.environment)
      internal                   = coalesce(var.boundary.loadbalancer.internal.internal, var.defaults.loadbalancer.internal.internal)
      load_balancer_type         = coalesce(var.boundary.loadbalancer.internal.load_balancer_type, var.defaults.loadbalancer.internal.load_balancer_type)
      security_groups            = try(coalescelist(var.boundary.loadbalancer.internal.security_groups, var.defaults.loadbalancer.internal.security_groups), [])
      subnets                    = coalescelist(var.boundary.loadbalancer.internal.subnets, var.defaults.loadbalancer.internal.subnets)
      enable_deletion_protection = coalesce(var.boundary.loadbalancer.internal.enable_deletion_protection, var.defaults.loadbalancer.internal.enable_deletion_protection)
    }
  }
}

resource "aws_lb" "loadbalancer_external" {
  name               = local.loadbalancer.external.name
  internal           = local.loadbalancer.external.internal
  load_balancer_type = local.loadbalancer.external.load_balancer_type
  security_groups = concat([aws_security_group.external_loadbalancer.id],
  local.loadbalancer.external.security_groups)
  subnets                    = local.loadbalancer.external.subnets
  enable_deletion_protection = local.loadbalancer.external.enable_deletion_protection

  access_logs {
    bucket  = aws_s3_bucket.lb_logs_external.id
    prefix  = local.loadbalancer.external.name
    enabled = true
  }

  tags = {
    Environment = local.loadbalancer.external.environment
  }
}

resource "aws_lb" "loadbalancer_internal" {
  name               = local.loadbalancer.internal.name
  internal           = local.loadbalancer.internal.internal
  load_balancer_type = local.loadbalancer.internal.load_balancer_type
  security_groups = concat([aws_security_group.internal_loadbalancer.id],
  local.loadbalancer.internal.security_groups)
  subnets                    = local.loadbalancer.internal.subnets
  enable_deletion_protection = local.loadbalancer.internal.enable_deletion_protection

  tags = {
    Environment = local.loadbalancer.external.environment
  }
}

resource "aws_s3_bucket" "lb_logs_external" {
  bucket = local.loadbalancer.external.name

  tags = {
    Name        = local.loadbalancer.external.name
    Environment = local.loadbalancer.external.environment
  }
}

data "aws_acm_certificate" "boundary_skao_int" {
  domain      = "boundary.skao.int"
  statuses    = ["ISSUED"]
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}

resource "aws_lb_listener" "listener_external" {
  load_balancer_arn = aws_lb.loadbalancer_external.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS13-1-2-2021-06"
  certificate_arn   = data.aws_acm_certificate.boundary_skao_int.arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.controller.arn
  }
}

resource "aws_lb_target_group" "controller" {
  name     = "controller"
  port     = 9200
  protocol = "HTTPS"
  vpc_id   = data.aws_subnet.controllers.vpc_id
}

