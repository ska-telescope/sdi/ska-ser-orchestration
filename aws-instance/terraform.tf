terraform {
  required_version = ">=1.7.5"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.0"
    }
    null = {
      source  = "hashicorp/null"
      version = "~>3.1.1"
    }
    external = {
      source  = "hashicorp/external"
      version = "~>2.2.2"
    }
  }
}
