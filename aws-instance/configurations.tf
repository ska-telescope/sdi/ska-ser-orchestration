locals {
  configuration = merge(var.configuration, {
    instance_type           = coalesce(var.configuration.instance_type, var.defaults.instance_type)
    iam_instance_profile    = try(coalesce(var.configuration.iam_instance_profile, var.defaults.iam_instance_profile), null)
    ami                     = coalesce(var.configuration.ami, var.defaults.ami)
    availability_zone       = coalesce(var.configuration.availability_zone, var.defaults.availability_zone)
    disable_api_termination = coalesce(var.configuration.disable_api_termination, var.defaults.disable_api_termination, true)
    subnet_id               = coalesce(var.configuration.subnet_id, var.defaults.subnet_id)
    keypair                 = coalesce(var.configuration.keypair, var.defaults.keypair)
    jump_host               = try(coalesce(var.configuration.jump_host, var.defaults.jump_host), null)
    vpc_security_group_ids  = coalesce(var.configuration.vpc_security_group_ids, var.defaults.vpc_security_group_ids)
    region                  = try(coalesce(var.configuration.region, var.defaults.region), null)
    root_block_device       = try(coalesce(var.configuration.root_block_device, var.defaults.root_block_device), null)
    skao_backup_daily       = coalesce(var.configuration.skao_backup_daily, var.defaults.skao_backup_daily)
    skao_backup_hourly      = coalesce(var.configuration.skao_backup_hourly, var.defaults.skao_backup_hourly)
    user_data               = try(coalesce(var.configuration.user_data, var.defaults.user_data), null)
    volumes                 = coalesce(var.configuration.volumes, var.defaults.volumes)
  })
}