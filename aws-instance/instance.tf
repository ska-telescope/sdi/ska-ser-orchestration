locals {
  user = "ubuntu" # TODO: Get from image metadata
}

resource "aws_instance" "instance" {
  ami                     = local.configuration.ami
  disable_api_termination = local.configuration.disable_api_termination
  instance_type           = local.configuration.instance_type
  iam_instance_profile    = local.configuration.iam_instance_profile
  key_name                = local.configuration.keypair
  subnet_id               = local.configuration.subnet_id
  user_data               = local.configuration.user_data
  vpc_security_group_ids  = local.configuration.vpc_security_group_ids
  root_block_device {
    delete_on_termination = local.configuration.root_block_device.delete_on_termination
    volume_size           = local.configuration.root_block_device.volume_size
    volume_type           = local.configuration.root_block_device.volume_type
  }
  dynamic "ebs_block_device" {
    for_each = {
      for key, value in local.configuration.volumes :
      key => value
    }
    content {
      delete_on_termination = ebs_block_device.value.delete_on_termination
      device_name           = ebs_block_device.value.name
      encrypted             = ebs_block_device.value.encrypted
      snapshot_id           = ebs_block_device.value.snapshot_id
      volume_type           = ebs_block_device.value.type
      volume_size           = ebs_block_device.value.size
      tags                  = ebs_block_device.value.tags
    }
  }

  tags = {
    Name             = local.configuration.name
    skaoBackupDaily  = local.configuration.skao_backup_daily
    skaoBackupHourly = local.configuration.skao_backup_hourly
  }

} 