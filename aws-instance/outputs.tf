output "instance" {
  description = "Instance state"
  value = {
    id              = aws_instance.instance.id
    name            = aws_instance.instance.tags.Name
    ami             = local.configuration.ami
    user            = local.user
    security_groups = local.configuration.security_groups
    subnet_id       = local.configuration.subnet_id
    network = {
      private = aws_instance.instance.private_ip
      public  = aws_instance.instance.public_ip
    }
    keypair = local.configuration.keypair
    region  = local.configuration.region

  }
}

output "inventory" {
  description = "Instance ansible inventory"
  value       = local.inventory
}