variable "defaults" {
  type = object({
    availability_zone       = string
    instance_type           = string
    iam_instance_profile    = optional(string)
    ami                     = string
    disable_api_termination = optional(bool, true)
    keypair                 = string
    region                  = optional(string)
    subnet_id               = string
    jump_host               = optional(string)
    elastic_ip_subnet_id    = optional(string)
    vpn_cidr_blocks         = optional(list(string))
    vpc_security_group_ids  = optional(list(string))
    root_block_device = optional(object({
      delete_on_termination = optional(bool, false)
      volume_size           = number
      volume_type           = optional(string)
    }))
    skao_backup_daily  = optional(bool, false)
    skao_backup_hourly = optional(bool, false)
    user_data          = optional(string)
    volumes = optional(list(object({
      name                  = string
      delete_on_termination = optional(bool, false)
      encrypted             = optional(bool, false)
      size                  = number
      snapshot_id           = optional(string)
      tags                  = optional(map(string), {})
      type                  = string
    })), [])
  })
  description = "Set of default values used when creating AWS instances"
}

variable "configuration" {
  type = object({
    name                     = string
    instance_type            = optional(string)
    iam_instance_profile     = optional(string)
    ami                      = optional(string)
    availability_zone        = optional(string)
    disable_api_termination  = optional(bool)
    region                   = optional(string)
    subnet_id                = optional(string)
    create_security_group    = optional(bool, true)
    security_groups          = optional(list(string), [])
    external_security_groups = optional(list(string), [])
    keypair                  = optional(string)
    jump_host                = optional(string)
    metadata                 = optional(map(string), {})
    create_port              = optional(bool, false)
    fixed_ip                 = optional(string)
    port_security_enabled    = optional(bool, true)
    vpc_security_group_ids   = optional(list(string), [])
    skao_backup_daily        = optional(bool)
    skao_backup_hourly       = optional(bool)
    root_block_device = optional(object({
      delete_on_termination = optional(bool, false)
      volume_size           = number
      volume_type           = optional(string)
    }))
    user_data = optional(string)
    volumes = optional(list(object({
      name                  = string
      delete_on_termination = optional(bool, false)
      encrypted             = optional(bool, false)
      size                  = number
      snapshot_id           = optional(string)
      tags                  = optional(map(string), {})
      type                  = string
    })))
    applications      = optional(list(string), [])
    create_elastic_ip = optional(bool)
    elastic_ip = optional(object({
      create    = optional(bool)
      address   = optional(string)
      subnet_id = optional(string)
    }))
  })
  description = "Instance configuration"
}
