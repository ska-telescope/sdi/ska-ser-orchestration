locals {
  inventory = {
    (aws_instance.instance.tags.Name) = {
      ansible_host               = aws_instance.instance.id
      ansible_user               = local.user
      ansible_python_interpreter = "python3"
      ip                         = aws_instance.instance.private_ip
      keypair                    = local.configuration.keypair
      region                     = local.configuration.region
    }
  }
}

# Null resource to store the inventory in the state without an output
resource "null_resource" "inventory" {
  triggers = {
    type      = "instance"
    inventory = base64encode(jsonencode(local.inventory))
  }
}